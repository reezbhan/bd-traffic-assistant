package com.example.rafiq.bd_traffic_assistent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;

/**
 * Created by Reezbhan Imteaz on 4/6/2016.
 */
public class Starting_activity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting);

        CountDownTimer timer = new CountDownTimer(2000, 1000) //.5 second Timer
        {
            public void onTick(long l)
            {

            }

            @Override
            public void onFinish()
            {
                try {
                    Intent i = new Intent(Starting_activity.this, MainActivity.class);
                    startActivity(i);
                }catch (Exception e){

                }

            };
        }.start();
    }
}
