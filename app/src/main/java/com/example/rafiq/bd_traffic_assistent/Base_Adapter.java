package com.example.rafiq.bd_traffic_assistent;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Rafiq on 01-Feb-16.
 */

class Country{
    int imgId;
    //String countryNames;
    public Country(int imgId){
        this.imgId = imgId;
        //this.countryNames = countryNames;
    }
}

class ViewHolder{
    ImageView myCountry;
    //TextView myTitle;
    ViewHolder(View v)
    {
        myCountry = (ImageView) v.findViewById(R.id.SingleimageView);
        //myTitle = (TextView) v.findViewById(R.id.Singletitle);
    }
}

public class Base_Adapter extends BaseAdapter {
    ArrayList<Country> list;
    Context context;
    public Base_Adapter(Context context){

        this.context = context;
        list = new ArrayList<Country>();
        //Resources res = context.getResources();
        //String[] tempCountryNames =  res.getStringArray(R.array.country_names);

        int[] image = {R.drawable.a,R.drawable.a1,R.drawable.a2,
                R.drawable.a3,R.drawable.a4,R.drawable.a5,
                R.drawable.a6, R.drawable.a7,
                R.drawable.a8,R.drawable.a9, R.drawable.a10,
                R.drawable.a11,R.drawable.a12, R.drawable.a13,R.drawable.a14
                ,R.drawable.a15, R.drawable.a16,R.drawable.a17
                ,R.drawable.a18,R.drawable.a19,R.drawable.untitled,R.drawable.untitled2,R.drawable.untitled3,R.drawable.untitled4,R.drawable.untitled5,R.drawable.untitled6
        ,R.drawable.untitled7,R.drawable.untitled8,R.drawable.untitled9,R.drawable.untitled10
        ,R.drawable.untitled11,R.drawable.untitled12};

        for (int i = 0; i <32 ; i++) {
            Country temp = new Country(image[i]);
            list.add(temp);
        }
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder = null;
        if (row==null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.single_item,parent,false);
            holder = new ViewHolder(row);
            row.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) row.getTag();
        }

        Country tempCountry = list.get(position);
        holder.myCountry.setImageResource(tempCountry.imgId);
        //holder.myTitle.setText(tempCountry.countryNames);
        holder.myCountry.setTag(tempCountry);
        return row;
    }
}
